function getRuleS(url) {
    let d = [];
    try {
        d = fetch(url)
        if (url.startsWith('hiker://page/'))
            d = JSON.parse(d).rule
        d = JSON.parse(d)
    } catch (e) {
        d = []
    }
    return d.filter((v) => { return v.title != MY_RULE.title && v.author != '轻合集生成器' })
}
function load(url) {
    return getRuleS(url);
}

var preurl = 'https://codeberg.org/52fhy/PublicRule/raw/branch/master/';
var myurls=[
    preurl+'fhykzt.json'
    ,'http://hiker.nokia.press/hikerule/dev/json_list?name=450384733' //晓
    ,'http://hiker.nokia.press/hikerule/dev/json_list?name=510381049' //逐风者
    ,'http://hiker.nokia.press/hikerule/dev/json_list?name=2579949378' //蓝莓
    ,'http://hiker.nokia.press/hikerule/dev/json_list?name=184462840' //回忆
    ,'https://git.tyrantg.com/tyrantgenesis/hikerViewRules/raw/master/data/rules.json' //TyrantG
    ,preurl+'fhyys.json'
];

function batchGet(urls){
    if (urls == undefined) {urls = myurls;}
    let d=[];
    for(url of urls){
        getRuleS(url).map(v=>{d.push(v)});
    }
    return d;
}

function getYum(){
    var yum = JSON.parse(fetch(preurl+'yum'));
    let d = [];
    d.push({
        title: '小程序',
        url: 'toast://点击即可导入小程序',
        col_type: 'text_1'
    });
    for(var i=0;i<yum.length;i++)
    {
        var r={};
        var k=yum[i];      
        r.title=k['title'];
        r.url='rule://海阔视界规则分享，当前分享的是：小程序@' + base64Encode(JSON.stringify(k));
        r.col_type='text_3';
        d.push(r);
    }

    let plugins = JSON.parse(fetch(preurl+'plugin.json'));
    d.push({
        title: '插件',
        url: 'toast://点击即可导入插件',
        col_type: 'text_1'
    });
    for(var i=0;i<plugins.length;i++)
    {
        var r={};
        var k=plugins[i];      
        r.title=k['title'];
        r.url= k['rule'];
        r.col_type='text_3';
        d.push(r);
    }

    return d;
}

function getYumS(){
    let d = getYum();
    d.push({
        title: '杂货铺',
        url: 'toast://点击即可导入小程序',
        col_type: 'text_1'
    });
    let batchRule = batchGet([preurl+"wz.json"]);
    for(v of batchRule){
        var r={};   
        r.title=v['title'];
        r.url='rule://海阔视界规则分享，当前分享的是：小程序@' + base64Encode(JSON.stringify(v));
        r.col_type='text_3';
        d.push(r);
    }
    return d;
}

function zero(s) {
  return s < 10 ? '0' + s : s;
};

function makeRuleBak(data){
    let mydate = new Date();
    let year = mydate.getFullYear();
    let month = mydate.getMonth() + 1;
    let day = mydate.getDate();
    let datestr = year + '' + zero(month) + '' + zero(day);
    writeFile('hiker://files/rules/bak/'+MY_RULE.title+'/bak_' + datestr +'.json',JSON.stringify(data));
}


function getWelcome(){
    var s2 = 'https://www.prlrr.com/php-api/Avatar.php#?glideCache=skip' + '#' + new Date().getTime();
    let mydate = new Date();
    let year = mydate.getFullYear();
    let month = mydate.getMonth() + 1;
    let data = mydate.getDate();
    let hour = mydate.getHours();
    let minute = mydate.getMinutes();
    let second = mydate.getSeconds();
    let day = new Array(" 星期日 ", "星期一 ", "星期三", "星期三", "星期四", "星期五", "星期六")[mydate.getDay()];
    let newTime = year + ' 年 ' + zero(month) + ' 🈷️ ' + zero(data) + ' 日     \n时间: ' + ' \t ' + zero(hour) + ' : ' + zero(minute) + ' : ' + zero(second) + '\t\t' + day;
    let urls = ["toast://没事不要瞎点！", "https://v.nrzj.vip/", "https://lanmeiguojiang.com/xiaojiejie", "https://lanmeiguojiang.com/web1/youxi.html", "http://sbeyer.github.io/2048/"];
    let hitokoto = '';
        try {
        hitokoto = JSON.parse(fetch('https://v1.jinrishici.com/all.json', { timeout: 3000 })).content;
        urls.push("toast://" + hitokoto);
    } catch (e) {
        log(e);
    }

    return {
        title: hitokoto,
        url: urls[Math.floor(Math.random()*urls.length)],
        desc: '🐂辛丑年' + newTime + '',
        col_type: 'movie_1_vertical_pic',
        pic_url: s2
    };
}